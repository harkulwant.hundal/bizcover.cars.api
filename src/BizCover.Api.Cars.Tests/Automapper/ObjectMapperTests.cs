using System;
using AutoMapper;
using BizCover.Api.Cars.Automapper;
using BizCover.Api.Cars.Models.Common;
using BizCover.Api.Cars.Tests.Common;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace BizCover.Api.Cars.Tests.Automapper
{
    public class ObjectMapperTests
    {
        public readonly IMapper _mapper;
        public ObjectMapperTests()
        {
            _mapper = MockHelper.GetAutomapperObject();
        }

        [Fact]
        public void WhenTransformApiCarObjectToRepositoryCarObject_ThenTransformShouldBeSuccessful()
        {
            var apiCarModel = new Car()
            {
                Id= -99,
                Make = "test-make",
                Model = "test-model",
                Year = 2000,
                CountryManufactured = "australia",
                Price = 10M
            };

            var repositoryCarModel = _mapper.Map<Car, BizCover.Repository.Cars.Car>(apiCarModel);

            Assert.Equal(apiCarModel.Id, repositoryCarModel.Id);
            Assert.Equal(apiCarModel.Make, repositoryCarModel.Make);
            Assert.Equal(apiCarModel.Model, repositoryCarModel.Model);
            Assert.Equal(apiCarModel.Year, repositoryCarModel.Year);
            Assert.Equal(apiCarModel.CountryManufactured, repositoryCarModel.CountryManufactured);
            Assert.Equal(apiCarModel.Price, repositoryCarModel.Price);
        }

        [Fact]
        public void WhenTransformRepositoryCarObjectToApiCarObject_ThenTransformShouldBeSuccessful()
        {
            var repositoryCarModel = new BizCover.Repository.Cars.Car()
            {
                Id= -99,
                Make = "test-make",
                Model = "test-model",
                Year = 2000,
                CountryManufactured = "australia",
                Price = 10M
            };

            var apiCarModel = _mapper.Map<BizCover.Repository.Cars.Car, Car>(repositoryCarModel);

            Assert.Equal(apiCarModel.Id, repositoryCarModel.Id);
            Assert.Equal(apiCarModel.Make, repositoryCarModel.Make);
            Assert.Equal(apiCarModel.Model, repositoryCarModel.Model);
            Assert.Equal(apiCarModel.Year, repositoryCarModel.Year);
            Assert.Equal(apiCarModel.CountryManufactured, repositoryCarModel.CountryManufactured);
            Assert.Equal(apiCarModel.Price, repositoryCarModel.Price);
        }
    }
}
