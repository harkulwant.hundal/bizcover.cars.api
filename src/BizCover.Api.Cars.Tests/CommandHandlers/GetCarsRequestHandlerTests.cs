
using System.Threading;
using System.Threading.Tasks;
using BizCover.Api.Cars.CommandHandlers;
using BizCover.Api.Cars.Tests.Common;
using BizCover.Repository.Cars;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace BizCover.Api.Cars.Tests.CommandHandlers
{
    public class GetCarsRequestHandlerTests
    {
        private readonly Mock<ICarRepository> _carRepositoryMock;
        private readonly Mock<ILogger<GetCarsRequestHandler>> _loggerMock;
        private readonly GetCarsRequestHandler _handler;
        public GetCarsRequestHandlerTests()
        {
          _carRepositoryMock = MockHelper.MockCarRepository();
          _loggerMock = MockHelper.MockLogger<GetCarsRequestHandler>();
          _handler = new GetCarsRequestHandler(_carRepositoryMock.Object, MockHelper.GetAutomapperObject(), _loggerMock.Object);
        }

        [Fact]
        public async Task WhenRequestCars_ThenAllCarsShouldBeReturnedSuccessfully()
        {
            var response = await _handler.Handle(new Models.Requests.GetCarsRequest(), CancellationToken.None);

            Assert.Equal(response.Cars.Count, MockHelper.GetCars().Count);
        }
    }
}
