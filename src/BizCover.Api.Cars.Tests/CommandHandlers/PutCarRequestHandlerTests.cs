
using System.Threading;
using System.Threading.Tasks;
using BizCover.Api.Cars.CommandHandlers;
using BizCover.Api.Cars.Tests.Common;
using BizCover.Repository.Cars;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace BizCover.Api.Cars.Tests.CommandHandlers
{
    public class PutCarRequestHandlerTests
    {
        private readonly Mock<ICarRepository> _carRepositoryMock;
        private readonly Mock<ILogger<PutCarRequestHandler>> _loggerMock;
        private readonly PutCarRequestHandler _handler;
        public PutCarRequestHandlerTests()
        {
          _carRepositoryMock = MockHelper.MockCarRepository();
          _loggerMock = MockHelper.MockLogger<PutCarRequestHandler>();
          _handler = new PutCarRequestHandler(_carRepositoryMock.Object, MockHelper.GetAutomapperObject(), _loggerMock.Object);
        }

        [Fact]
        public async Task WhenNewCarIsPUT_ThenCarShouldBeSavedSuccessfully()
        {
            var car = new Models.Common.Car
            {
                Id = 0,
                Make = "test-make-new",
                Model = "test-model-new",
                Year = 2000,
                CountryManufactured = "australia",
                Price = 1M
            };

            var response = await _handler.Handle(new Models.Requests.PutCarRequest() {
                Car = car
            }, CancellationToken.None);

            Assert.True(response.Id >= MockHelper.FirstRandomForNewCar);
        }


        [Fact]
        public async Task WhenExistingCarIsPUT_ThenCarShouldBeUpdatedSuccessfully()
        {
            var car = new Models.Common.Car
            {
                Id = 99,
                Make = "test-make-existing",
                Model = "test-model-existing",
                Year = 2000,
                CountryManufactured = "australia",
                Price = 1M
            };

            var response = await _handler.Handle(new Models.Requests.PutCarRequest() {
                Car = car
            }, CancellationToken.None);

            Assert.Equal(response.Id, car.Id);
        }
    }
}
