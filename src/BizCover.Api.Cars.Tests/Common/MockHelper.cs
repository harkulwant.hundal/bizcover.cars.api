using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BizCover.Api.Cars.Automapper;
using BizCover.Api.Cars.Extensions;
using BizCover.Api.Cars.Models.Purchase;
using BizCover.Api.Cars.RulesEngine.Calculators;
using BizCover.Repository.Cars;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;

namespace BizCover.Api.Cars.Tests.Common
{
    public class MockHelper
    {
        public const int FirstRandomForNewCar = 100000;
        public const int LastRandomForNewCar = 999999;

        public static Mock<ILogger<T>> MockLogger<T>()
        {
            var mock = new Mock<ILogger<T>>();
            return mock;
        }

        public static Mock<ICarRepository> MockCarRepository(List<Car> cars = null)
        {
            var mock = new Mock<ICarRepository>();
            
            // hydrate list of cars
            if(cars == null)
            {
                cars = GetCars();
            }

            mock
                .Setup(s => s.GetAllCars())
                .ReturnsAsync(cars);

            var random = new System.Random();
            mock
                .Setup(s => s.Add(It.IsAny<Car>()))
                .ReturnsAsync(random.Next(FirstRandomForNewCar, LastRandomForNewCar));

            mock
                .Setup(s => s.Update(It.IsAny<Car>()));
            
            return mock;
        }

        public static IMapper GetAutomapperObject()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddAutoMapper(typeof(BizCoverMapProfile));
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var mapper = serviceProvider.GetService<IMapper>();
            return mapper;
        }

        public static Mock<IDiscountCalculator> MockDiscountCalculator()
        {
            var mock = new Mock<IDiscountCalculator>();

            mock
                .Setup(s => s.Calculate(It.IsAny<List<CarOrder>>()));

            return mock;
        }

        public static List<Car> GetCars()
        {
            return new List<Car>()
            {
                new Car() 
                {
                    Id= 1,
                    Make = "test-make-1",
                    Model = "test-model-1",
                    Year = 1990,
                    CountryManufactured = "australia",
                    Price = 10M
                },
                new Car() 
                {
                    Id= 2,
                    Make = "test-make-2",
                    Model = "test-model-2",
                    Year = 2000,
                    CountryManufactured = "australia",
                    Price = 10M
                },
                new Car() 
                {
                    Id= 3,
                    Make = "test-make-3",
                    Model = "test-model-3",
                    Year = 2010,
                    CountryManufactured = "australia",
                    Price = 20M
                },
                new Car() 
                {
                    Id= 4,
                    Make = "test-make-4",
                    Model = "test-model-4",
                    Year = 2015,
                    CountryManufactured = "japan",
                    Price = 30M
                },
                new Car() 
                {
                    Id= 5,
                    Make = "test-make-5",
                    Model = "test-model-5",
                    Year = 2020,
                    CountryManufactured = "italy",
                    Price = 80M
                }
                
            };
        }

        public static List<CarOrder> GetCarOrders(List<Car> cars = null)
        {
            var mapper = GetAutomapperObject();

            if(cars == null)
            {
                var dbCars = GetCars();
            }

            var apiCarModels = mapper.Map<List<Car>, List<Models.Common.Car>>(cars);
            return apiCarModels.Select(x => new CarOrder() { Car = x }).ToList(); 
        }


        public static IEnumerable<Discount> DiscountsApplied<T>(List<CarOrder> carOrders, decimal discount)
        {
            var discounts = carOrders
            .SelectMany(c => c.Discounts)
            .Where(d => string.Equals(d.Type, typeof(T).Name) && 
                        string.Equals(d.Percentage, discount.DisplayPercentage()));

            return discounts;
        }
    }
}
