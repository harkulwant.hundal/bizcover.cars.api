

using System.Collections.Generic;
using System.Linq;
using BizCover.Api.Cars.Extensions;
using BizCover.Api.Cars.Models.Purchase;

namespace BizCover.Api.Cars.RulesEngine.DiscountRules
{

  ///<summary>
  /// If number of cars is more than 2, apply 3% discount
  ///</summary>
  public class TotalQuantityDiscountRule : IDiscountRule
  {
    const decimal DiscountPercentage = 3M;

    public void Evaluate(List<CarOrder> carOrders)
    {
        var totalQuantity = carOrders.Count();
        
        if(totalQuantity > 2)
        {
          foreach(var carOrder in carOrders)
          {
            carOrder.Discounts.Add(new Models.Purchase.Discount() {
              Type = this.GetType().Name,
              Percentage = DiscountPercentage.DisplayPercentage(),
              Less = carOrder.Car.Price.ApplyDiscount(DiscountPercentage)
            });
          }
        }
    }
  }
}