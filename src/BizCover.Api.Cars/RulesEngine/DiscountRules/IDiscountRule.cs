

using System.Collections.Generic;
using BizCover.Api.Cars.Models.Purchase;

namespace BizCover.Api.Cars.RulesEngine.DiscountRules
{
  public interface IDiscountRule
  {
      void Evaluate(List<CarOrder> carOrders);
  }
}