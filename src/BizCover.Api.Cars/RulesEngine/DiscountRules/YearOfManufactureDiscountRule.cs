

using System.Collections.Generic;
using System.Linq;
using BizCover.Api.Cars.Extensions;
using BizCover.Api.Cars.Models.Purchase;

namespace BizCover.Api.Cars.RulesEngine.DiscountRules
{

  ///<summary>
  /// If car year is before 2000, apply 10% discount
  ///</summary>
  public class YearOfManufactureDiscountRule : IDiscountRule
  {
    const decimal DiscountPercentage = 10M;

    public void Evaluate(List<CarOrder> carOrders)
    {
      foreach(var carOrder in carOrders)
      {
        if(carOrder.Car.Year < 2000)
        {
          carOrder.Discounts.Add(new Models.Purchase.Discount() {
            Type = this.GetType().Name,
            Percentage = DiscountPercentage.DisplayPercentage(),
            Less = carOrder.Car.Price.ApplyDiscount(DiscountPercentage)
          });
        }
      }
        
    }
  }
}