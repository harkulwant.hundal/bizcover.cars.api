

using System;
using System.Collections.Generic;
using System.Linq;
using BizCover.Api.Cars.Models.Purchase;

namespace BizCover.Api.Cars.RulesEngine.DiscountRules
{

  ///<summary>
  /// Combine all discounts for a car
  ///</summary>
  public class ConsolidateDiscountRule : IDiscountRule
  {
    public void Evaluate(List<CarOrder> carOrders)
    {
      foreach(var carOrder in carOrders)
      {
        var discountTotal = carOrder.Discounts.Sum(x => x.Less);
        carOrder.Amount = Math.Round((carOrder.Car.Price - discountTotal), 4);
      }
        
    }
  }
}