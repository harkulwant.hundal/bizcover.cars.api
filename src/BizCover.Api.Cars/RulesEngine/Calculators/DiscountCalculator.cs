

using System.Collections.Generic;
using System.Linq;
using BizCover.Api.Cars.Extensions;
using BizCover.Api.Cars.Models.Purchase;
using BizCover.Api.Cars.RulesEngine.DiscountRules;

namespace BizCover.Api.Cars.RulesEngine.Calculators
{

  public class DiscountCalculator : IDiscountCalculator
  {
    private readonly List<IDiscountRule> _rules;

    // use autofac for iindex to get rule implementations
    public DiscountCalculator()
    {
      _rules = new List<IDiscountRule>();

      _rules.Add(new TotalCostDiscountRule());
      _rules.Add(new TotalQuantityDiscountRule());
      _rules.Add(new YearOfManufactureDiscountRule());
      
      _rules.Add(new ConsolidateDiscountRule());
    }
    public void Calculate(List<CarOrder> carOrders)
    {
      foreach(var rule in _rules)
      {
        rule.Evaluate(carOrders);
      }
    }
  }
}