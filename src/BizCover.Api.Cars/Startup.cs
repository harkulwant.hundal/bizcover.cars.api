﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.IO;
using BizCover.Api.Cars.Filters;
using MediatR;
using AutoMapper;
using BizCover.Api.Cars.Automapper;
using BizCover.Api.Cars.RulesEngine.Calculators;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace BizCover.Api.Cars
{
    public class Startup
    {
        const string JsonContentType = "application/json";
        protected IWebHostEnvironment WebHostEnvironment { get; set; }
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            WebHostEnvironment = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore()
                .AddFluentValidation();
            
            services.AddAutoMapper(typeof(BizCoverMapProfile).Assembly);

            services.AddMediatR(typeof(Startup));

            services.AddHttpContextAccessor();
            services.AddLogging();
            services.AddOptions();

            services.AddTransient<Repository.Cars.ICarRepository, Repository.Cars.CarRepository>();
            services.AddTransient<IDiscountCalculator, DiscountCalculator>();

            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = true;
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
            });

            services.AddVersionedApiExplorer(o => 
            {
                // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
                // note: the specified format code will format the version as "'v'major[.minor][-status]
                o.GroupNameFormat= "'v'VVV";

                // note: this option is only necessary when versioning by url segment.
                // The substitutionFormat can also be used to control the format of the API version in route templates
                o.SubstituteApiVersionInUrl = true;
            });

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                // note: need a temporary service provider here because one has not been created yet
                var provider = services.BuildServiceProvider()
                    .GetRequiredService<IApiVersionDescriptionProvider>();
                
                var titleSuffix = !string.Equals(WebHostEnvironment.EnvironmentName, "production", StringComparison.OrdinalIgnoreCase) 
                    ? $" :: {WebHostEnvironment.EnvironmentName?.ToUpper()}"
                    : string.Empty;

                // add a swagger document for each discovered API verison
                foreach(var description in provider.ApiVersionDescriptions)
                {
                    c.SwaggerDoc(description.GroupName, new OpenApiInfo
                    {
                        Title = $"BizCover Cars API{titleSuffix}",
                        Version = description.GroupName,
                        Description = ".NET CORE 3.1 Web API service to manage cars",
                        // TermsOfService = new Uri("http://creditstar.com.au/terms/leadboard"),
                        Contact = new OpenApiContact 
                            { 
                                Name = "Harkulwant Hundal", 
                                Email = "harkulwant.hundal@mjsiver.com" 
                            }
                    });
                }
                
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    Scheme = "Bearer",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        },
                        Scheme = "oauth2",
                        Name = "Bearer",
                        In = ParameterLocation.Header,

                        },
                        new List<string>()
                    }
                    });

                // apply the filters
                c.OperationFilter<RemoveVersionFromParameterFilter>();
                c.DocumentFilter<ReplaceVersionWithExactValueInPathFilter>();
                
                c.CustomSchemaIds(x => x.FullName);

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, ILogger<Startup> logger, IWebHostEnvironment env, IApiVersionDescriptionProvider apiVersionDescriptionProvider)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = string.Empty;
                foreach(var description in apiVersionDescriptionProvider.ApiVersionDescriptions)
                {
                    c.SwaggerEndpoint(
                        $"/swagger/{description.GroupName}/swagger.json",
                        description.GroupName.ToLower()
                    );
                }
            });

            if (string.Equals(env.EnvironmentName, "development", StringComparison.OrdinalIgnoreCase))
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            // extend exception middleware for custom error types
            app.UseExceptionHandler(error => {
                error.Run(async context => {
                    context.Response.ContentType = JsonContentType;
                    var exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();

                    logger.Log(LogLevel.Error, exceptionHandlerFeature.Error, exceptionHandlerFeature.Error.Message);
                    var errorObj = new {
                        Error = exceptionHandlerFeature.Error.Message
                    };

                    await context.Response.WriteAsync(JsonConvert.SerializeObject(errorObj));
                });
            });

            app.UseRouting();
            app.UseStaticFiles();
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });

            // app.UseHttpsRedirection();
        }
    }
}
