
using System.Linq;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.OpenApi.Models;

namespace BizCover.Api.Cars.Filters
{

  public class RemoveVersionFromParameterFilter : IOperationFilter
  {
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
      var versionParameter = operation.Parameters.SingleOrDefault(p => p.Name == "version");
      
      if(versionParameter != null)
        operation.Parameters.Remove(versionParameter);
        
    }
  }
}