
using System.Linq;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.OpenApi.Models;

namespace BizCover.Api.Cars.Filters
{

  public class ReplaceVersionWithExactValueInPathFilter : IDocumentFilter
  {
    public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
    {
      var paths = swaggerDoc.Paths
        .ToDictionary(
          path => path.Key.Replace("v{version}", swaggerDoc.Info.Version),
          path => path.Value
        );

      var placeholderPaths = new OpenApiPaths();

      foreach(var path in paths)
        placeholderPaths.Add(path.Key, path.Value);

      swaggerDoc.Paths = placeholderPaths;
        
    }
  }
}