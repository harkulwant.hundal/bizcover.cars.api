
using System.Collections.Generic;
using BizCover.Api.Cars.Models.Common;

namespace BizCover.Api.Cars.Models.Responses
{

  public class GetCarsResponse
  {
    public List<Car> Cars { get; set; }
  }
}