

using BizCover.Api.Cars.Models.Responses;
using MediatR;

namespace BizCover.Api.Cars.Models.Requests
{

  public class GetCarsRequest : IRequest<GetCarsResponse>
  {
    
  }
}