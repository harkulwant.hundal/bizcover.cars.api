
using System.Collections.Generic;
using BizCover.Api.Cars.Models.Responses;
using MediatR;

namespace BizCover.Api.Cars.Models.Requests
{

  public class PurchaseCarsRequest : IRequest<PurchaseCarsResponse>
  {
    public List<int> CarIds { get; set; }
  }
}