

using BizCover.Api.Cars.Models.Common;
using BizCover.Api.Cars.Models.Responses;
using MediatR;

namespace BizCover.Api.Cars.Models.Requests
{

  public class PutCarRequest : IRequest<PutCarResponse>
  {
    public Car Car { get; set; }
  }
}