
namespace BizCover.Api.Cars.Models.Common
{
  public class Car
  {
    public int Id { get; set; }
    public string Make { get; set; }
    public string Model { get; set; }
    public int Year { get; set; }
    public string CountryManufactured { get; set; }
    public string Colour { get; set; }
    public decimal Price { get; set; }

    public override string ToString()
    {
      return $"{Make} {Model} - {Year}";
    }
  }
}