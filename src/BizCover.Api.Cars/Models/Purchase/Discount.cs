
namespace BizCover.Api.Cars.Models.Purchase
{
  public class Discount
  {
    public string Type { get; set; }
    public string Percentage { get; set; }
    public decimal Less { get; set; }
  }
}