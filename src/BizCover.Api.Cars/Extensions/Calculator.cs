

using System;

namespace BizCover.Api.Cars.Extensions
{
  public static class Calculator
  {
      public static decimal ApplyDiscount (this decimal amount, decimal discount)
      {
        if(discount == 0)
          return 0M;

        return Math.Round((amount * discount / 100), 4);
      }

      public static string DisplayPercentage (this decimal discount)
      {
        return $"{(discount/100):P}";
      }
  }
}