

using System.Threading;
using System.Threading.Tasks;
using BizCover.Api.Cars.Models.Requests;
using BizCover.Api.Cars.Models.Responses;
using MediatR;
using BizCover.Repository.Cars;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using BizCover.Api.Cars.RulesEngine.Calculators;
using BizCover.Api.Cars.Models.Purchase;
using System;

namespace BizCover.Api.Cars.CommandHandlers
{

  public class PurchaseCarsRequestHandler : IRequestHandler<PurchaseCarsRequest, PurchaseCarsResponse>
  {
    private readonly ICarRepository _carRepository;
    private readonly IDiscountCalculator _discountCalculator;
    private readonly IMapper _mapper;
    private readonly ILogger<PurchaseCarsRequestHandler> _logger;

    public PurchaseCarsRequestHandler(ICarRepository carRepository, 
      IDiscountCalculator discountCalculator,
      IMapper mapper, 
      ILogger<PurchaseCarsRequestHandler> logger)
    {
      _carRepository = carRepository;
      _discountCalculator = discountCalculator;
      _mapper = mapper;
      _logger = logger;
    }

    public async Task<PurchaseCarsResponse> Handle(PurchaseCarsRequest request, CancellationToken cancellationToken)
    {
      var dbCars = await _carRepository.GetAllCars();
      var dbFilteredCars = dbCars.Where(x => request.CarIds.Contains(x.Id)).ToList();
      var filteredCars = _mapper.Map<List<Car>, List<Models.Common.Car>>(dbFilteredCars);

      var carOrders = filteredCars.Select(x => new CarOrder() { Car = x }).ToList();

      _discountCalculator.Calculate(carOrders);

      return new PurchaseCarsResponse()
      {
        CarOrders = carOrders,
        Total = Math.Round((carOrders.Sum(x => x.Amount)), 4),
        Count = filteredCars.Count(),
        Discount = Math.Round((carOrders.SelectMany(x => x.Discounts).Sum(x => x.Less)), 4)
      };
    }
  }
}