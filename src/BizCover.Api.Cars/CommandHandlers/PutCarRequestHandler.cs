

using System.Threading;
using System.Threading.Tasks;
using BizCover.Api.Cars.Models.Requests;
using BizCover.Api.Cars.Models.Responses;
using MediatR;
using BizCover.Repository.Cars;
using AutoMapper;
using Microsoft.Extensions.Logging;

namespace BizCover.Api.Cars.CommandHandlers
{

  public class PutCarRequestHandler : IRequestHandler<PutCarRequest, PutCarResponse>
  {
    private readonly ICarRepository _carRepository;
    private readonly IMapper _mapper;
    private readonly ILogger<PutCarRequestHandler> _logger;

    public PutCarRequestHandler(ICarRepository carRepository, IMapper mapper, ILogger<PutCarRequestHandler> logger)
    {
      _carRepository = carRepository;
      _mapper = mapper;
      _logger = logger;
    }

    public async Task<PutCarResponse> Handle(PutCarRequest request, CancellationToken cancellationToken)
    {
      var dbCar = _mapper.Map<Models.Common.Car, Repository.Cars.Car>(request.Car);
      var response = new PutCarResponse() { Id = dbCar.Id };

      // upsert
      if(request.Car.Id > 0){

        // todo: countryManufactured is currently not getting updated.
        await _carRepository.Update(dbCar);

      }
      else {
        response.Id = await _carRepository.Add(dbCar);
      }

      return response;
    }
  }
}