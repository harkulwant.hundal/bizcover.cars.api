﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Microsoft.Extensions.Logging;
using BizCover.Api.Cars.Models.Common;
using System.Net;
using BizCover.Api.Cars.Models.Requests;
using System.Threading.Tasks;

namespace BizCover.Api.Cars.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        private readonly IMediator _mediatR;
        private readonly ILogger<CarsController> _logger;
        public CarsController(IMediator mediator, ILogger<CarsController> logger)
        {
            _mediatR = mediator;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<Car>), (int)HttpStatusCode.OK)]
        public async  Task<ActionResult> Get()
        {
            var response = await _mediatR.Send(new GetCarsRequest());
            return Ok(response.Cars);
        }

        [HttpPut]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Accepted)]
        public async Task<ActionResult> Put(Car model)
        {
            var response = await _mediatR.Send(new PutCarRequest() { Car = model });
            return Accepted(response.Id);
        }

        [HttpPost]
        [Route("purchase")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> Purchase(List<int> ids)
        {
            var response = await _mediatR.Send(new PurchaseCarsRequest() { CarIds = ids });
            return Accepted(response);
        }
    }
}
